/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */
//#include <stdint.h>
#include <asf.h>
#include <avr/interrupt.h>
//#include "ow_delay.h"
#include "ds1820.h"

#define UART_BUF_SIZE 128

#define	UART_CMD_GET_V				(0x0)
#define	UART_CMD_GET_V_ALL			(0xf)

#define	UART_CMD_GET_T_ROM			(0x0)
#define	UART_CMD_GET_T_OW			(0x1)
#define	UART_CMD_GET_T_ROM_ALL		(0xe)
#define	UART_CMD_GET_T_OW_ALL		(0xf)
#define UART_CMD_T_CONVERT			(0xc)
#define UART_CMD_UPDATE_OW			(0xd)

#define UART_CMD_GET_OW_DEVICE_NUM			(0x0)
#define UART_CMD_GET_OW_DEVICE_ROM			(0x1)
#define UART_CMD_GET_OW_DEVICE_USER_BYTES	(0x2)

#define ADC_REQ_E2_UPDATE_MASK				(1<<0)
#define ADC_REQ_CONV_START_MASK				(1<<1)
#define ADC_REQ_GET_TMP_MASK				(1<<2)
#define ADC_REQ_GET_TMP_ALL_ROM_MASK		(1<<3)
#define ADC_REQ_GET_TMP_ALL_OW_MASK			(1<<4)
#define ADC_REQ_UPDATE_OW_MASK				(1<<5)

const char info_string[] = "telemetry_avr(v0.3a)\n";

volatile struct adc_t {
	uint8_t channels[BOARD_ADC_CHANNELS_NUM];
	uint16_t shadow_results[BOARD_ADC_CHANNELS_NUM];
	uint16_t results[BOARD_ADC_CHANNELS_NUM];
	uint8_t current_channel	:3;
	uint8_t started			:1;
	uint8_t finished		:1;
} adc = {
	.channels			=  BOARD_ADC_CHANNELS,
	.current_channel	= 0,
	.started			= 0,
	.finished			= 0
};

volatile struct periodic_timer_t {
	uint16_t period;
	uint8_t start;
} periodic_timer = {
	.period = DEFAULT_SCAN_PERIOD,
	.start	= 0
};

volatile struct uart_t {
	uint8_t buf[UART_BUF_SIZE];
	uint8_t buflen;
	uint8_t send_cnt;
	uint8_t started;
	uint8_t finished;
}uart = {
	.buflen		= 0,
	.send_cnt		= 0,
	.started	= 0,
	.finished	= 0
};

#define DS_MAX_DEV (16)
volatile struct {
	uint8_t rom_size;
	uint16_t temperature[DS_MAX_DEV];
	uint16_t user_byte[DS_MAX_DEV];
	uint8_t dev;
	uint16_t val;
	uint8_t reqest;
}ds1820 = {
	.rom_size=0,
	.user_byte[0]=0,
	.user_byte[1]=0,
	.reqest =0
};

uint8_t __temp_read_rom_buf[9];

enum uart_fsm_state_t {UART_STATE_START=0, UART_STATE_GET_GROUP, UART_STATE_GET_CH, UART_STATE_STOP};

uint8_t adc_start(void);
uint8_t periodic_timer_start(void);

void uart_send_char(uint8_t c);
uint8_t hexascii_to_uint8 (uint8_t ch, uint8_t* res);
void uart_fsm(uint8_t ch, uint8_t reset);
uint8_t uart_send_data(uint8_t type, uint8_t group, uint8_t channel);
uint8_t uart_send_start(const uint8_t* buf, uint8_t len);

void reset_handler(uint8_t res);
void tx_handler(uint8_t len);

int main (void)
{
	uint8_t data[60];
	/* Insert system clock initialization code here (sysclk_init()). */
	//adc.finished? led_on(BOARD_MASK_LED_PG1) : led_off(BOARD_MASK_LED_PG1);
	board_init();
	//ow_init(&reset_handler,NULL,NULL);
	//ow_init();
	ds1820_init();
	periodic_timer_start();
	/* Insert application code here, after the board has been initialized. */
	ds1820_convert_t (NULL);
	ds1820.rom_size= ds1820_rom_search();
	
	for (uint8_t dev=0; dev<ds1820.rom_size; dev++){
		ds1820.user_byte[dev]  = ds1820_e2_read(ds1820_rom_found_get(dev));
	}

	while (1) {
		if (adc.finished) {
			adc.finished=0;
		}
		// Update: e2 prom ds1820
		if (ds1820.reqest & ADC_REQ_E2_UPDATE_MASK){
			ds1820.reqest&=~ADC_REQ_E2_UPDATE_MASK;
			if (ds1820.val) led_on(BOARD_MASK_LED_PG1);
			else led_off(BOARD_MASK_LED_PG1);
			ds1820_e2_write(ds1820.val, ds1820_rom_found_get(ds1820.dev));
			for (uint8_t dev=0; dev<ds1820.rom_size; dev++){
				ds1820.user_byte[dev]  = ds1820_e2_read(ds1820_rom_found_get(dev));
			}
		}
		if (ds1820.reqest & ADC_REQ_GET_TMP_MASK){
			ds1820.reqest&=~ADC_REQ_GET_TMP_MASK;
			ds1820.temperature[ds1820.dev]  = ds1820_get_t(ds1820_rom_found_get(ds1820.dev));
			data[0]= ds1820.temperature[ds1820.dev] >> 8;
			data[1]= ds1820.temperature[ds1820.dev] & 0xFF;
			uart_send_start(data,2);
			/*for (uint8_t dev=0; dev<ds1820.rom_size; dev++){
				ds1820.temperature[dev]  = ds1820_get_t(ds1820_rom_found_get(dev));
			}*/
		}
		if (ds1820.reqest & ADC_REQ_GET_TMP_ALL_ROM_MASK){
			ds1820.reqest&=~ADC_REQ_GET_TMP_ALL_ROM_MASK;
			data[0] = ds1820.rom_size;
			for (uint8_t i=0; i<ds1820.rom_size;i++){
				data[(i<<1)+1]= (uint8_t)(ds1820.user_byte[i] & 0xff);
				ds1820.temperature[i] = ds1820_get_t(ds1820_rom_found_get(i));
				data[(i<<1)+2]= (uint8_t)((ds1820.temperature[i]>>1) & 0xff);
			}
			uart_send_start(data,(ds1820.rom_size<<1)+1);
		}
		if (ds1820.reqest & ADC_REQ_GET_TMP_ALL_OW_MASK){
			ds1820.reqest&=~ADC_REQ_GET_TMP_ALL_OW_MASK;
			data[0] = ds1820.rom_size;
			for (uint8_t i=0; i<ds1820.rom_size;i++){
				data[(i<<1)+1]= i;
				ds1820.temperature[i] = ds1820_get_t(ds1820_rom_found_get(i));
				data[(i<<1)+2]= (uint8_t)((ds1820.temperature[i]>>1) & 0xff);
			}
			uart_send_start(data,(ds1820.rom_size<<1)+1);
		}
		if (ds1820.reqest & ADC_REQ_CONV_START_MASK){
			ds1820.reqest&=~ADC_REQ_CONV_START_MASK;
			ds1820_convert_t (NULL);
			data[0]=1;
			uart_send_start(data,1);
		}
		if (ds1820.reqest & ADC_REQ_UPDATE_OW_MASK){
			ds1820.reqest&=~ADC_REQ_UPDATE_OW_MASK;
			ds1820.rom_size= ds1820_rom_search();
			for (uint8_t dev=0; dev<ds1820.rom_size; dev++)
				ds1820.user_byte[dev]  = ds1820_e2_read(ds1820_rom_found_get(dev));
			data[0]=1;
			uart_send_start(data,1);
		}
		
	}
}

uint8_t adc_start(void){
	uint8_t ret=0;
	//uint8_t sreg_save = SREG;
	
	//cli();
	if (!adc.started){
		adc.current_channel = 0;
		ADMUX = (ADMUX & ~BOARD_ADC_ADMUX_MASK) | adc.channels[adc.current_channel];
		ADCSRA |= 1<<ADSC;		// Start Conversion
		adc.started = 1;
		ret=1;
	}
	//SREG = sreg_save;
	return ret;
}

uint8_t periodic_timer_start(void){
	
	TCNT1=periodic_timer.period;;//��������� �������� �������
	TCCR1B|=(1<<CS12)|(1<<CS10);//������������ = 1024
	return 0;
}

void uart_send_char(uint8_t c){//   �������� �����
	while(!(UCSR0A & (1<<UDRE))) {}; //  ���������������, ����� ������� ��������
	UDR0 = c;
}

uint8_t uart_send_start(const uint8_t* buf, uint8_t len){
	if (!uart.started) {
		if (len>sizeof(uart.buf)) return 0;
		for (uint8_t i=0; i < len; i++){
			uart.buf[i]	= buf[i];
		}
		uart.buflen = len;
		uart.send_cnt = 0;
		uart.started = 1;
		uart0_send_irq_enable();
	}
	else 
		return 0;
	
	return 1;
}

void uart_fsm(uint8_t ch, uint8_t reset){
	static enum uart_fsm_state_t state;
	static enum uart_fsm_state_t next_state = UART_STATE_START;
	static uint8_t type, group, channel;
	
	state = next_state;
	
	switch(state){
		case UART_STATE_START:
			next_state = UART_STATE_GET_GROUP;
			if (ch=='t' || ch=='T') type='t';
			else if (ch=='v' || ch=='V') type='v';
			else if (ch=='w' || ch=='W') type='w';
			else if (ch=='e' || ch=='E') type='e';
			else if (ch=='i' || ch=='I') type='i';
			else next_state = UART_STATE_START;
			break;
		case UART_STATE_GET_GROUP:
			if (hexascii_to_uint8(ch, &group))
				next_state = UART_STATE_GET_CH;
			else
				next_state = UART_STATE_START;
			break;
		case UART_STATE_GET_CH:
			if (hexascii_to_uint8(ch, &channel))
				next_state = UART_STATE_STOP;
			else
				next_state = UART_STATE_START;
			break;
		case UART_STATE_STOP:
			if (ch=='n' || ch=='N')
				uart_send_data(type, group,channel);
			next_state = UART_STATE_START;
			break;
		default:
			;
	}
	
}

uint8_t hexascii_to_uint8 (uint8_t ch, uint8_t* res){
	uint8_t ret=1;
	if (ch>=0x30 && ch<=0x39) 
		*res = ch & 0x0f;
	else if (ch>=0x61 && ch<=0x66) 
		*res = ch-0x61+0x0a;
	else if (ch>=0x41 && ch<=0x46) 
		*res = ch-0x41+0x0a;
	else 
		ret=0;
	
	return ret;
}

uint8_t uart_send_data(uint8_t type, uint8_t group, uint8_t channel){
	//led_toggle(BOARD_MASK_LED_PG1);
	uint8_t ch=0;
	uint8_t data[10];
	/* Temperature function */
	if (type=='t'){
		switch (group){
			case UART_CMD_GET_T_ROM:
				/*while (ch<ds1820.rom_size){
					if (ds1820.user_byte[ch]==channel) break;
					ch++;
				}
				if (ch<ds1820.rom_size){
					data[0]= ds1820.temperature[ch] >> 8;
					data[1]= ds1820.temperature[ch] & 0xFF;
				} else
					data[0]=data[1]=0; //for (uint8_t i=0;i<8;i++) data[i]=0;*/
				
				while (ch<ds1820.rom_size){
					if (ds1820.user_byte[ch]==channel) break;
					ch++;
				}
				if (ch<ds1820.rom_size){
					ds1820.dev=ch;
					ds1820.reqest|=ADC_REQ_GET_TMP_MASK;
				}
				else {
					data[0]=data[1]=0;
					uart_send_start(data,2);
				}
				//uart_send_start(data,2);
				break;
			case UART_CMD_GET_T_OW:
				/*data[0]= ds1820.temperature[channel] >> 8;
				data[1]= ds1820.temperature[channel] & 0xFF;
				uart_send_start(data,2);*/
				if (channel<ds1820.rom_size){
					ds1820.dev=channel;
					ds1820.reqest|=ADC_REQ_GET_TMP_MASK;
				} else {
					data[0]=data[1]=0;
					uart_send_start(data,2);
				}
				break;
			case UART_CMD_GET_T_ROM_ALL:
				ds1820.reqest|=ADC_REQ_GET_TMP_ALL_ROM_MASK;
				break;
			case UART_CMD_GET_T_OW_ALL:
				/*data[0] = ds1820.rom_size;
				for (uint8_t i=0; i<ds1820.rom_size;i++){
					data[(i<<1)+1]= (group==UART_CMD_GET_T_OW_ALL)? i: (uint8_t)(ds1820.user_byte[i] & 0xff);
					data[(i<<1)+2]= (uint8_t)((ds1820.temperature[i]>>1) & 0xff);
				}
				uart_send_start(data,(ds1820.rom_size<<1)+1);*/
				ds1820.reqest|=ADC_REQ_GET_TMP_ALL_OW_MASK;
				break;
			case UART_CMD_T_CONVERT:
				ds1820.reqest|=ADC_REQ_CONV_START_MASK;
				break;
			case UART_CMD_UPDATE_OW:
				ds1820.reqest|=ADC_REQ_UPDATE_OW_MASK;
				break;
			default:
				data[0]=data[1]=0;
				uart_send_start(data,2);
		}
		
	}
	/* Voltmeter function */
	else if(type=='v'){
		if (group==UART_CMD_GET_V && channel < BOARD_ADC_CHANNELS_NUM){
			data[0] = (adc.results[channel] >> 8) & 0xff;
			data[1] = adc.results[channel] & 0xff;
			uart_send_start(data,2);
		} else if (group==UART_CMD_GET_V_ALL){
			for(uint8_t i=0;i<BOARD_ADC_CHANNELS_NUM; i++){
				data[i<<1]		= (adc.results[i] >> 8) & 0xff;
				data[(i<<1)+1]	= adc.results[i] & 0xff;
			}
			uart_send_start(data,BOARD_ADC_CHANNELS_NUM*2);
		} else {
			data[0]=data[1]=0;
			uart_send_start(data,2);
		}
	}
	else if(type=='w'){
		if (channel<ds1820_rom_found_get_size()){
			ds1820.dev=channel;
			ds1820.val=group;
			ds1820.reqest|=ADC_REQ_E2_UPDATE_MASK;
			data[0]=1;
		} else data[0]=0;
		uart_send_start(data,1);
	}
	else if(type=='e'){
		if (group==UART_CMD_GET_OW_DEVICE_NUM) {
			data[0]=ds1820_rom_found_get_size();
			uart_send_start(data,1);
		}
		else if (group==UART_CMD_GET_OW_DEVICE_ROM){
			const uint8_t* p = ds1820_rom_found_get(channel);
			if (p!=NULL) uart_send_start(p,8);
			else {
				for (uint8_t i=0;i<8;i++) data[i]=0;
				uart_send_start(data,8);
			}
		}
		else if (group==UART_CMD_GET_OW_DEVICE_USER_BYTES){
			if (channel<ds1820_rom_found_get_size()){
				data[0] = (ds1820.user_byte[channel] >> 8) & 0xff;
				data[1] = ds1820.user_byte[channel] & 0xff;
			}else 
				data[0]=data[1]=0;
			uart_send_start(data,2);
		}
	} else if(type=='i'){
		uart_send_start(info_string ,sizeof(info_string)-1);
	}
	return 0;
}


/*ADC Conversion Complete Interrupt Service Routine (ISR)*/
ISR(ADC_vect)
{
	adc.shadow_results [adc.current_channel++] = ADC;
	if (adc.current_channel < BOARD_ADC_CHANNELS_NUM){
		ADMUX = (ADMUX & ~BOARD_ADC_ADMUX_MASK) | adc.channels[adc.current_channel];
		ADCSRA |= 1<<ADSC;		// Start Conversion
	}
	else {
		for(uint8_t i=0; i < BOARD_ADC_CHANNELS_NUM; i++){
			adc.results[i] = adc.shadow_results[i];
		}
		adc.started = 0;
		adc.finished = 1;
	}
}

ISR(TIMER1_OVF_vect)
{
	led_toggle(BOARD_MASK_LED_PG0);
	adc_start();
	
	TCNT1=periodic_timer.period;//��������� �������� �������
}

ISR(USART0_RX_vect){
	//led_toggle(BOARD_MASK_LED_PG1);
	uart_fsm(UDR0,0);
}

ISR(USART0_UDRE_vect){
	
	if (uart.send_cnt < uart.buflen){
		UDR0 = uart.buf[uart.send_cnt++];
	}
	else {
		uart0_send_irq_disable();
		uart.started =0;
		uart.finished =1;
	}
}

