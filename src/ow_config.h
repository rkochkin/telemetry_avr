/*
 * ow_config.h
 *
 * Created: 27.09.2018 14:17:39
 *  Author: user
 */ 


#ifndef OW_CONFIG_H_
#define OW_CONFIG_H_

#define TX_LOW()	(PORTD &= ~(1<<PD3))
#define TX_BRAKE()	(PORTD |= (1<<PD3))
#define RX()		(PIND & (1<<PIND2))
#define GPIO_INIT() {DDRD &= ~(1<<DDD2); DDRD |= (1<<DDD3); PORTD |= (1<<PD3);}
// Prescaler 8
#define TIMER_INIT() {TCCR0=(1<<CS01)|(1<<WGM01);}
#define DELAY(us) {	cli(); OCR0  = us; TCNT0 = 0; TIFR = (1<<OCF0); sei();	while (!(TIFR & (1<<OCF0)) ){};}

#endif /* OW_CONFIG_H_ */