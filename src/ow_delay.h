/*
 * ow_delay.h
 *
 * Created: 22.09.2018 12:43:26
 *  Author: user
 */ 


#ifndef OW_DELAY_H_
#define OW_DELAY_H_


#define OW_FOSC				(8000000)
#define OW_IRQ_TIMER_COMP	TIMER3_COMPA_vect

#define OW_BUF_LEN		(16)

typedef enum  {
	OW_TIME_RESET_PULSE=480,
	OW_TIME_RESET_PAUSE=70,
	OW_TIME_RESET_PRESENSE=420,
	
	OW_TIME_RX_START=2,
	OW_TIME_RX=15,
	OW_TIME_RX_PAUSE=60,
	
	OW_TIME_TX_START=1,
	OW_TIME_TX=60,
	OW_TIME_TX_PAUSE=1
	
}ow_time;

typedef struct {
	//ow_fsm_state_t state;
	uint8_t presense :1;
	uint8_t bit_cnt;
	uint8_t buf[OW_BUF_LEN];
	uint8_t buf_cnt;
	uint8_t buf_len;
	
	uint8_t started :1;
	uint8_t timer_finish :1;
	
	//ow_reset_complete_handler_t reset_handler;
	//ow_rx_complete_handler_t rx_handler;
	//ow_tx_complete_handler_t tx_handler;
} ow_t;

void ow_init(void);
uint8_t ow_reset_presense(void);
void ow_tx_byte(uint8_t data);
uint8_t ow_tx(const uint8_t* buf, uint8_t tx_len, uint8_t buf_len);
uint8_t ow_rx(uint8_t* buf, uint8_t rx_len, uint8_t buf_len);
uint8_t ow_search_all(uint8_t* buf, uint16_t buf_len);
uint8_t ow_crc8(const uint8_t* buf, uint8_t len);

#endif /* OW_DELAY_H_ */