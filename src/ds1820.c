/*
 * ds1820.c
 *
 * Created: 22.09.2018 12:16:54
 *  Author: user
 */ 

#include "stdint.h"
#include "stddef.h"
#include "ow_delay.h"
#include "ds1820.h"

uint8_t data_buf[16];
static ds1820_scratchpad_t __scratchpad;

void ds1820_init(void){
	ow_init();
}

ds1820_t __ds1820 ={
	.rom_size =0
};

static void rom_match_cmd(const uint8_t* rom){
	if (rom != NULL){
		ow_tx_byte (DS1820_MATCH_ROM);
		ow_tx (rom,DS1820_ROM_SIZE, DS1820_ROM_SIZE);
	} 
	else 
		ow_tx_byte(DS1820_SKIP_ROM);
}

uint8_t ds1820_convert_t(const uint8_t* rom){
	if (ow_reset_presense()){
		rom_match_cmd(rom);
		ow_tx_byte(DS1820_CONVERT_T);
		ow_rx(data_buf,1,sizeof(data_buf));
	} else return 0;
	return 1;
}


uint8_t ds1820_scratchpad_read(ds1820_scratchpad_t* scratchpad, const uint8_t* rom){
	if (!ow_reset_presense()) return 0;
	
	rom_match_cmd(rom);
	ow_tx_byte(DS1820_READ_SCRATCHPAD);
	ow_rx((uint8_t*) scratchpad, DS1820_SCRATCHPAD_SIZE, DS1820_SCRATCHPAD_SIZE);
	if (ow_crc8((uint8_t*) scratchpad, DS1820_SCRATCHPAD_SIZE-1)!=scratchpad->crc) return 0;
	ow_reset_presense();
	
	return DS1820_SCRATCHPAD_SIZE;
}

uint8_t ds1820_scratchpad_write(const ds1820_scratchpad_t* scratchpad , const uint8_t* rom){
	if (ow_reset_presense()){
		rom_match_cmd(rom);
		ow_tx_byte(DS1820_WRITE_SCRATCHPAD);
		ow_tx_byte(scratchpad->th_user_byte_1);
		ow_tx_byte(scratchpad->tl_user_byte_2);
		ow_reset_presense();
	} else return 0;
	return 2;
}

uint8_t ds1820_scratchpad_copy(const uint8_t* rom){
	if (ow_reset_presense()){
		rom_match_cmd(rom);
		ow_tx_byte(DS1820_COPY_SCRATCHPAD);
		ow_reset_presense();
	} else return 0;
	return 1;
}

uint16_t ds1820_get_t(const uint8_t* rom){
	uint16_t res=0;
	if (ds1820_scratchpad_read(&__scratchpad, rom)) {
		res = __scratchpad.temperature_lsb;						//  buf[DS1820_SPBYTE_TEMPERATURE_LSB] & 0xFF;			// TEMPERATURE LSB
		res |= (uint16_t)__scratchpad.temperature_msb <<8;		//(uint16_t)buf[DS1820_SPBYTE_TEMPERATURE_MSB] << 8;	// TEMPERATURE MSB
	}
	return res;
}

uint16_t ds1820_e2_read(const uint8_t* rom){
	uint16_t ret=0;
	if (ds1820_scratchpad_read(&__scratchpad, rom)){
		ret = __scratchpad.tl_user_byte_2;					// data_buf[DS1820_SPBYTE_TL_USER_BYTE_2] & 0xFF; //
		ret|= (uint16_t)__scratchpad.th_user_byte_1<<8;		//(uint16_t)data_buf[DS1820_SPBYTE_TH_USER_BYTE_1]<< 8; //
	}
	return ret;
}

uint8_t ds1820_e2_write(uint16_t user_byte, const uint8_t* rom){
	__scratchpad.tl_user_byte_2 = (uint8_t)user_byte;
	__scratchpad.th_user_byte_1 = (uint8_t)(user_byte>>8);
	if (ds1820_scratchpad_write(&__scratchpad,rom)){
		ds1820_scratchpad_copy(rom);
	} else return 0;
	return DS1820_SCRATCHPAD_WRITE_SIZE;
}

uint8_t ds1820_rom_read(uint8_t* buf){
	if(ow_reset_presense()){
		ow_tx_byte (DS1820_READ_ROM);
		ow_rx((uint8_t*)buf,DS1820_ROM_SIZE,DS1820_ROM_SIZE);
	} else return 0;
	return DS1820_ROM_SIZE;
}

uint8_t ds1820_rom_search(void){
	
	 return __ds1820.rom_size = ow_search_all(__ds1820.rom, sizeof(__ds1820.rom));
}

uint8_t ds1820_rom_found_get_size(){
	return __ds1820.rom_size;
}

const uint8_t* ds1820_rom_found_get(uint8_t num){
	if (num >= __ds1820.rom_size) return NULL;
	return &(__ds1820.rom[num<<3]);
}