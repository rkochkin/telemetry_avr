/**
 * \file
 *
 * \brief User board configuration template
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#ifndef CONF_BOARD_H
#define CONF_BOARD_H

/* UART to HOST set baudrate (bit/s)*/
#define BOARD_UART_BAUD				(19200)

/* Scan period (us)*/
#define DEFAULT_SCAN_PERIOD_MS		(1000)

#endif // CONF_BOARD_H
