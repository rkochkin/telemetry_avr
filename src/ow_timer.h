#if 0


/*
 * ow.h
 *
 * Created: 20.09.2018 16:39:34
 *  Author: Roman Kochkin <electromanko@gmail.com>
 */ 


#ifndef OW_TIMER_H_
#define OW_TIMER_H_

#define OW_FOSC				(8000000)
#define OW_IRQ_TIMER_COMP	TIMER3_COMPA_vect

#define OW_BUF_LEN		(16)

typedef void (*ow_reset_complete_handler_t)(uint8_t);
typedef void (*ow_rx_complete_handler_t)(uint8_t*,uint8_t);
typedef void (*ow_tx_complete_handler_t)(uint8_t);

typedef enum  {
				OW_TIME_RESET_PULSE=480,
				OW_TIME_RESET_PAUSE=70,
				OW_TIME_RESET_PRESENSE=420,
				
				OW_TIME_RX_START=2,
				OW_TIME_RX=12,
				OW_TIME_RX_PAUSE=60,
				
				OW_TIME_TX_START=2,
				OW_TIME_TX=70,
				OW_TIME_TX_PAUSE=2
				
		}ow_time;

typedef enum  {	
						OW_STATE_IDLE=0,
						
						OW_STATE_RESET_START,
						OW_STATE_RESET_PAUSE, 
						OW_STATE_RESET_PRESENSE,
						OW_STATE_RESET_COMPLETE,
						
						OW_STATE_RX_START,
						OW_STATE_RX,
						OW_STATE_RX_SAMPLE,
						OW_STATE_RX_COMPLETE,
						
						OW_STATE_TX_START,
						OW_STATE_TX,
						OW_STATE_TX_PAUSE,
						OW_STATE_TX_COMPLETE
						
} ow_fsm_state_t;

typedef struct {
	ow_fsm_state_t state;
	uint8_t presense :1;
	uint8_t bit_cnt;
	uint8_t buf[OW_BUF_LEN];
	uint8_t buf_cnt;
	uint8_t buf_len;
	
	ow_reset_complete_handler_t reset_handler;
	ow_rx_complete_handler_t rx_handler;
	ow_tx_complete_handler_t tx_handler;
} ow_t;

void ow_init(ow_reset_complete_handler_t reset_handler, ow_rx_complete_handler_t rx_handler, ow_tx_complete_handler_t tx_handler);


uint8_t ow_reset(void);
uint8_t ow_tx_byte(uint8_t* buf, uint8_t len);
uint8_t ow_rx_byte(uint8_t len);

#endif /* OW_TIMER_H_ */

#endif