/*
 * ds1820.h
 *
 * Created: 22.09.2018 12:16:23
 *  Author: user
 */ 


#ifndef DS1820_H_
#define DS1820_H_

#define DS1820_ROM_SIZE					(8)
#define DS1820_SCRATCHPAD_SIZE			(9)
#define DS1820_SCRATCHPAD_WRITE_SIZE	(2)
#define DS1820_MAX_ROM_DEVICE			(10)

typedef enum {
	DS1820_MATCH_ROM = 0x55,
	DS1820_READ_ROM = 0x33,
	DS1820_SKIP_ROM = 0xCC,
	DS1820_CONVERT_T = 0x44,
	DS1820_READ_SCRATCHPAD = 0xBE,
	DS1820_WRITE_SCRATCHPAD = 0x4E,
	DS1820_COPY_SCRATCHPAD = 0x48
} ds1820_cmd_t;

typedef struct {
	uint8_t rom[DS1820_MAX_ROM_DEVICE*DS1820_ROM_SIZE];
	uint8_t rom_size;
} ds1820_t;

typedef uint8_t ds1820_rom_t[DS1820_ROM_SIZE];

typedef enum {
	DS1820_SPBYTE_TEMPERATURE_LSB=0,
	DS1820_SPBYTE_TEMPERATURE_MSB=1,
	DS1820_SPBYTE_TH_USER_BYTE_1=2,
	DS1820_SPBYTE_TL_USER_BYTE_2=3,
	DS1820_SPBYTE_RESERVED_0,
	DS1820_SPBYTE_RESERVED_1,
	DS1820_SPBYTE_COUNT_REMAIN,
	DS1820_SPBYTE_COUNT_PER_C,
	DS1820_SPBYTE_CRC
} ds1820_scratchpad_tenum;

typedef union {
	struct{
		uint8_t temperature_lsb; //TEMPERATURE_LSB,
		uint8_t temperature_msb; //DS1820_SPBYTE_TEMPERATURE_MSB=1,
		uint8_t th_user_byte_1;//DS1820_SPBYTE_TH_USER_BYTE_1=2,
		uint8_t tl_user_byte_2;//DS1820_SPBYTE_TL_USER_BYTE_2=3,
		uint8_t reserved_0;//DS1820_SPBYTE_RESERVED_0,
		uint8_t reserved_1;//DS1820_SPBYTE_RESERVED_1,
		uint8_t count_remain;//DS1820_SPBYTE_COUNT_REMAIN,
		uint8_t count_per_c;//DS1820_SPBYTE_COUNT_PER_C,
		uint8_t crc;//DS1820_SPBYTE_CRC
	};
	uint8_t bytes[DS1820_SCRATCHPAD_SIZE];
} ds1820_scratchpad_t;

void ds1820_init(void);
uint8_t ds1820_convert_t(const uint8_t* rom);
uint8_t ds1820_scratchpad_read(ds1820_scratchpad_t* scratchpad, const uint8_t* rom);
uint8_t ds1820_scratchpad_write(const ds1820_scratchpad_t* scratchpad , const uint8_t* rom);
uint8_t ds1820_scratchpad_copy(const uint8_t* rom);
uint16_t ds1820_get_t(const uint8_t* rom);
uint8_t ds1820_rom_search(void);
uint8_t ds1820_rom_found_get_size(void);
const uint8_t* ds1820_rom_found_get(uint8_t num);
uint8_t ds1820_rom_read(uint8_t* buf);
uint16_t ds1820_e2_read(const uint8_t* rom);
uint8_t ds1820_e2_write(uint16_t user_byte, const uint8_t* rom);


#endif /* DS1820_H_ */