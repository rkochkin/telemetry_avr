/**
 * \file
 *
 * \brief User board initialization template
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>

void adc_init(void);
void uart_init(void);
void periodic_timer_init(void);

void board_init(void)
{
	/* This function is meant to contain board-specific initialization code
	 * for, e.g., the I/O pins. The initialization can rely on application-
	 * specific board configuration, found in conf_board.h.
	 */
	adc_init();
	uart_init();
	periodic_timer_init();
	// Config outputs
	BOARD_DDR_LED_PGS	|= BOARD_MASK_LED_PG0 | BOARD_MASK_LED_PG1;
	BOARD_PORT_LED_PGS	&= ~(BOARD_MASK_LED_PG0 | BOARD_MASK_LED_PG1);
	sei();
}

void adc_init(void)
{
	BOARD_ADC_DDR &= ~(BOARD_ADC_PORT_MASK);
	BOARD_ADC_PORT &= ~(BOARD_ADC_PORT_MASK);
		
	// AREF = AVcc
	ADMUX = (1<<REFS0);
	    
	// ADC Enable and prescaler of 128
	// 8000000/128 = 62500
	ADCSRA = (1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
}

void uart_init(void)
{
	
//	Set baud rate
UBRR0H = (uint8_t)(BOARD_UART_UBRR>>8);
UBRR0L = (uint8_t) BOARD_UART_UBRR;

// Enable receiver and transmitter, RX Complete Interrupt Enable
UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);

//Set frame format: 8data, 1stop bit
UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);
}

void periodic_timer_init(void){
	
	TIMSK|=(1<<TOIE1);//��������� ���������� �� ������������ ������� 1
}