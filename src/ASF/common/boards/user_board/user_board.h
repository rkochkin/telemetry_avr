/**
 * \file
 *
 * \brief User board definition template
 *
 */

 /* This file is intended to contain definitions and configuration details for
 * features and devices that are available on the board, e.g., frequency and
 * startup time for an external crystal, external memory devices, LED and USART
 * pins.
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#ifndef USER_BOARD_H
#define USER_BOARD_H

#include <stdint.h>
#include <avr/io.h>
#include <conf_board.h>

// External oscillator settings.
// Uncomment and set correct values if external oscillator is used.

// External oscillator frequency
//#define BOARD_XOSC_HZ          8000000

// External oscillator type.
//!< External clock signal
//#define BOARD_XOSC_TYPE        XOSC_TYPE_EXTERNAL
//!< 32.768 kHz resonator on TOSC
//#define BOARD_XOSC_TYPE        XOSC_TYPE_32KHZ
//!< 0.4 to 16 MHz resonator on XTALS
//#define BOARD_XOSC_TYPE        XOSC_TYPE_XTAL

// External oscillator startup time
//#define BOARD_XOSC_STARTUP_US  500000

/* Status Register */
#ifndef SREG
#  if __AVR_ARCH__ >= 100
#    define SREG _SFR_MEM8(0x3F)
#  else
#    define SREG _SFR_IO8(0x3F)
#  endif
#endif

#define BOARD_XOSC_HZ					(8000000)
#define BOARD_DDR_LED_PGS				(DDRG)
#define BOARD_PORT_LED_PGS				(PORTG)
#define BOARD_MASK_LED_PG0				(1<<PORT0)
#define BOARD_MASK_LED_PG1				(1<<PORT1)

#define BOARD_ADC_DDR					(DDRF)
#define BOARD_ADC_PORT					(PORTF)
#define BOARD_ADC_PORT_MASK				(0x0f)

#define BOARD_ADC_ADMUX_MASK			(0x1f)
#define BOARD_ADC_ADMUX_CH0				(0x00)
#define BOARD_ADC_ADMUX_CH1				(0x01)
#define BOARD_ADC_ADMUX_CH2				(0x02)
#define BOARD_ADC_ADMUX_CH3				(0x03)

#define BOARD_ADC_CHANNELS_NUM			(4)
#define BOARD_ADC_CHANNELS				{BOARD_ADC_ADMUX_CH0, BOARD_ADC_ADMUX_CH1, BOARD_ADC_ADMUX_CH2, BOARD_ADC_ADMUX_CH3}

#define BOARD_UART_UBRR					(BOARD_XOSC_HZ/16/BOARD_UART_BAUD-1)

#define DEFAULT_SCAN_PERIOD			(0xffff - (DEFAULT_SCAN_PERIOD_MS *( BOARD_XOSC_HZ/1024))/1000 )

static inline void led_toggle(uint8_t mask) //uint8_t
{
	/*
	 * For older megaAVR devices read-modify-write PORT register.
	 * This isn't safe for interrupts.
	 */
	BOARD_PORT_LED_PGS ^= mask;
}

static inline void led_on(uint8_t mask) //uint8_t
{
	BOARD_PORT_LED_PGS |= mask;
}

static inline void led_off(uint8_t mask) //uint8_t
{
	BOARD_PORT_LED_PGS &= ~mask;
}

static inline void uart0_send_irq_enable(void){
	UCSR0B |= (1<<UDRIE0);
}

static inline void uart0_send_irq_disable(void){
	UCSR0B &= ~(1<<UDRIE0);
}
#endif // USER_BOARD_H
