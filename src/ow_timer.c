#if 0
#include "avr/io.h"
#include "avr/interrupt.h"
#include "stddef.h"
#include "ow_timer.h"

ow_t ow = {
	.rx_handler=NULL,
	.tx_handler=NULL,
	.state=OW_STATE_IDLE
	};



static void fsm(void);
static void timer_init(void);
static void gpio_init(void);
static void timer_start(uint16_t us);
static void timer_stop(void);
static void tx_low(void);
static void tx_brake(void);
static uint8_t rx(void);

/**************************************

		OW API

**************************************/
uint8_t ow_reset(void){
	if (ow.state==OW_STATE_IDLE){
		ow.state=OW_STATE_RESET_START;
		fsm();
		return 1;
	}
	return 0;
}

uint8_t ow_tx_byte(uint8_t* buf, uint8_t len){
	if (len<1 || len>OW_BUF_LEN) return 0;
	ow.bit_cnt=0;
	ow.buf_cnt=0;
	ow.buf_len=len;
	for (uint8_t i; i<len; i++){
		ow.buf[i]=buf[i];
	}
	ow.state=OW_STATE_TX_START;
	fsm();
	return 1;
}

uint8_t ow_rx_byte(uint8_t len){
	if (len<1 || len>OW_BUF_LEN) return 0;
	ow.bit_cnt=0;
	ow.buf_cnt=0;
	ow.buf_len=len;
	for (uint8_t i; i<len; i++){
		ow.buf[i]=0;
	}
	ow.state=OW_STATE_RX_START;
	fsm();
	return 1;
}

void ow_init(ow_reset_complete_handler_t reset_handler, ow_rx_complete_handler_t rx_handler, ow_tx_complete_handler_t tx_handler){
	
	ow.reset_handler = reset_handler;
	ow.rx_handler = rx_handler;
	ow.tx_handler = tx_handler;
	
	gpio_init();
	timer_init();
}

/************************************/

void fsm(void){
	
	switch(ow.state){
		case OW_STATE_IDLE:
			break;
///////////////////////////////////////////////////			
// OW RESET
///////////////////////////////////////////////////
		case OW_STATE_RESET_START:
			tx_low();
			ow.state = OW_STATE_RESET_PAUSE;
			timer_start(OW_TIME_RESET_PULSE);
			break;
			
		case OW_STATE_RESET_PAUSE:
			tx_brake();
			ow.state = OW_STATE_RESET_PRESENSE;
			timer_start(OW_TIME_RESET_PAUSE);
			break;
			
		case OW_STATE_RESET_PRESENSE:
			ow.presense = rx()? 0:1;
			ow.state = OW_STATE_RESET_COMPLETE;
			timer_start(OW_TIME_RESET_PRESENSE);
			break;
			
		case OW_STATE_RESET_COMPLETE:
			timer_stop();
			ow.state = OW_STATE_IDLE;
			if (ow.reset_handler!=NULL) ow.reset_handler(ow.presense);
			break;
			
///////////////////////////////////////////////////		
// OW RX
///////////////////////////////////////////////////
		case OW_STATE_RX_START:
			tx_low();
			ow.state=OW_STATE_RX;
			timer_start(OW_TIME_RX_START);
			break;
			
		case OW_STATE_RX:
			tx_brake();
			ow.state=OW_STATE_RX_SAMPLE;
			timer_start(OW_TIME_RX);
			break;
			
		case OW_STATE_RX_SAMPLE:
			if (rx()) ow.buf[ow.buf_cnt] |= (0x80 >>  ow.bit_cnt);
			ow.bit_cnt++;
			if (ow.bit_cnt>=8){
				ow.bit_cnt=0;
				ow.buf_cnt++;
				if (ow.buf_cnt>=ow.buf_len){
					ow.state=OW_STATE_RX_COMPLETE;
				} else {
					ow.state=OW_STATE_RX_START;
				}
			}
			timer_start(OW_TIME_RX_PAUSE);
			break;
			
		case OW_STATE_RX_COMPLETE:
			timer_stop();
			ow.state = OW_STATE_IDLE;
			if (ow.rx_handler!=NULL) 
				ow.rx_handler(ow.buf, ow.buf_len);
			break;
			
///////////////////////////////////////////////////			
// OW TX			
///////////////////////////////////////////////////
		case OW_STATE_TX_START:
			tx_low();
			ow.state=OW_STATE_TX;
			timer_start(OW_TIME_TX_START);
			break;
		
		case OW_STATE_TX:
			if (ow.buf[ow.buf_cnt] & (0x80 >> ow.bit_cnt)){
				tx_brake();
			}
			ow.state = OW_STATE_TX_PAUSE;
			timer_start(OW_TIME_TX);
			break;
			
		case OW_STATE_TX_PAUSE:
			ow.bit_cnt++;
			if (ow.bit_cnt >= 8){
				ow.bit_cnt=0;
				ow.buf_cnt++;
				if (ow.buf_cnt>=ow.buf_len){
					ow.state=OW_STATE_TX_COMPLETE;
					} else {
					ow.state=OW_STATE_TX_START;
				}
			}
			ow.state = OW_STATE_TX_COMPLETE;
			timer_start(OW_TIME_TX_PAUSE);
			break;
			
		case OW_STATE_TX_COMPLETE:
			timer_stop();
			ow.state = OW_STATE_IDLE;
			if (ow.tx_handler!=NULL)
				ow.tx_handler(ow.buf_len);
			break;
		default:;
	}
}

void timer_init(void){
	TCCR3B |= (1<<WGM32); // CTC mode
	ETIMSK|=(1<<OCIE3A); //��������� ���������� �� ���������� ������� 3
}

void timer_start(uint16_t us){
	TCNT3=0;
	OCR3A = us;
	TCCR3B|=(1<<CS31); //������������ �� 8
}

void timer_stop(void){
	TCCR3B=0;
}

void gpio_init(void){
	DDRD &= ~(1<<DDD2);
	DDRD |= (1<<DDD3);
	PORTD |= (1<<PD3);
}

void tx_low(void){
	PORTD &= ~(1<<PD3);
}

void tx_brake(void){
	PORTD |= (1<<PD3);
}

uint8_t rx(void){
	return (PIND & (1<<PIND2));
}

ISR(OW_IRQ_TIMER_COMP){
	fsm();
}
#endif